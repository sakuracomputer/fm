# coding: utf-8
require 'pry'

def exec?
  ARGV.include?('-e')
end

def has_client?(file)
  filename = File.basename(file)
  dirname = File.dirname(file)
  
  if filename =~ /[cC]lient/
    true
  else
    file_contents = File.open(file).read
    if file_contents =~ /[cC]lient/
      true
    else
      false
    end
  end
  
end

def rename_to_company_from_to_client(file)
  filename = File.basename(file)
  dirname = File.dirname(file)
  destname = replace_string(filename)
  destpath = File.join(dirname, destname)
  
  if filename != destname

    puts file + " -> #{destpath}"          
    if exec?
      File.rename(file, destpath)
    end
  end
end

def replace_to_company_from_client(file)
  file_contents = File.open(file).read
  replace_contents = replace_string(file_contents)

  if file_contents != replace_contents

    puts file

    if exec?
      File.write(file, replace_contents)
    end
  end
  
rescue => exception
  puts "rescue at #{file}."
  puts exception
end

def replace_string(string)
  string.gsub(/([cC])lients/, '\1ompanies')
    .gsub(/([cC])lient/, '\1ompany')
    .gsub(/クライアント企業/, "会社")           
    .gsub(/クライアント/, "会社")  
end

all_files = []
  
Dir.glob('./app/**/*.{rb,haml}').each do |f|
  all_files << f
end

Dir.glob('./config/**/*.{rb,yml}').each do |f|
  all_files << f
end

Dir.glob('./lib/**/*.{rb,haml}').each do |f|
  all_files << f
end

Dir.glob('./db/**/*.rb').each do |f|
  all_files << f
end

Dir.glob('./spec/{models,controllers,factories}/**/*.rb').each do |f|
  all_files << f
end

Dir.glob('./spec/dummy/app/**/*.{rb,haml}').each do |f|
  all_files << f
end

Dir.glob('./spec/dummy/spec/{models,controllers,factories}/**/*.rb').each do |f|
  all_files << f
end

Dir.glob('./spec/dummy/config/**/*.{rb,yml}').each do |f|
  all_files << f
end

Dir.glob('./spec/dummy/lib/**/*.{rb,haml}').each do |f|
  all_files << f
end

Dir.glob('./spec/dummy/db/**/*.rb').each do |f|
  all_files << f
end

target_files = []

all_files.each do |f|
  if has_client?(f)
    target_files << f
  end
end

puts "all file count:#{all_files.size}. target file count: #{target_files.size}."

puts "============================================================================ contents replaced"
target_files.each do |f|
  replace_to_company_from_client(f)
end

puts "============================================================================ file name renamed"
target_files.each do |f|
  rename_to_company_from_to_client(f)  
end
