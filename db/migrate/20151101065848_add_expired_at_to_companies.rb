class AddExpiredAtToCompanies < ActiveRecord::Migration
  def change
    add_column :fm_companies, :expired_at, :date
  end
end
