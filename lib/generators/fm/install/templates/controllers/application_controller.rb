# coding: utf-8
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action { RecordWithOperator.operator = current_user}

  def access_denied(exception)
    if current_user.try(:company).present?
      redirect_to fm.company_path(current_user.company.identifier), alert: exception.message
    end
  end

  def authenticate_admin_user!
    # for activeadmin
    authenticate_user!
    
    unless current_user.root?
      if current_user.try(:company).present?
        redirect_to fm.company_path(current_user.company.identifier), alert: 'お探しのページは存在しませんでした。'
      else
        redirect_to main_app.root_path
      end
    end
  end 
end
