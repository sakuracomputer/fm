class Users::PasswordsController < Devise::PasswordsController
  before_filter :configure_sign_in_params, only: [:create]
  before_filter {
    if params[:identifier].present?
      @company = Company.find(params[:identifier])
    end
  }
  
  # GET /resource/password/new
  def new
    super
    self.resource.company = @company
  end

  # POST /resource/password
  def create
    super do |u|
      u.company = @company
      u.company_id = @company.try(:id)
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  # def update
  #   super
  # end

  protected
  def configure_sign_in_params
    devise_parameter_sanitizer.for(:password) << :company_id
  end

  # def after_resetting_password_path_for(resource)
  #   super(resource)
  # end

  # The path used after sending reset password instructions
  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   super(resource_name)
  # end
end
