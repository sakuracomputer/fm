class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    cannot :manage, :all
    
    if user.root?
      can :manage, :all
      can :manage, ActiveAdmin::Page, name: "Dashboard"
    else
      if user.company_admin?
        can :manage, Fm::User, company_id: user.company_id
        can :manage, Fm::Company, id: user.company_id
      else
        can :read, Fm::Company
      end
    end
  end
  
end

