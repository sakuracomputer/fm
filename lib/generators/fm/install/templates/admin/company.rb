# coding: utf-8
ActiveAdmin.register Company do

  permit_params do
    Company.column_names
  end
  
  menu priority: 2, label: "会社"

  index do
    selectable_column
    column :id
    column :name
    column :identifier
    column :company_root do |com|
      link_to fm.company_path(com.identifier), fm.company_url(com.identifier), target: :_blank
    end
    
    column :contact
    column :email
    column :expired_at
    column :updated_at
    column :updated_user

    actions defaults: false do |c|
      link = link_to(t('active_admin.view'), admin_company_path(c))
      link = link + '　'
      link = link + link_to(t('active_admin.edit'),edit_admin_company_path(c))
      link
    end

  end

  filter :name
  filter :identifier
  filter :contact
  filter :email
  filter :updated_at
  filter :updated_user
  
  show do |company|
    attributes_table do
      row :id
      row :name
      row :identifier
      row :contact
      row :email
      row :expired_at
      row :tel1
      row :tel2
      row :fax
      row :zip
      row :address
      row :information
      row :updated_user
      row :updated_at
      row :created_user
      row :created_at
    end
  end

  form do |f|
    columns do
      column do
        f.inputs "会社情報" do
          f.input :name, hint: '日本語名'
          f.input :identifier, hint: '会社ルートページのURLに使用されます'
          f.input :expired_at, as: :datepicker, hint: '使用制限を設定する際に入力'
        end
      end
      column do; end
    end
    columns do
      column do
        f.inputs "連絡先" do
          f.input :contact
          f.input :email
          f.input :tel1, hint: '半角数字（ハイフンあり）'
          f.input :tel2, hint: '半角数字（ハイフンあり）'
          f.input :fax, hint: '半角数字（ハイフンあり）'
        end
      end
      column do
        f.inputs "住所" do
          f.input :zip, hint: '半角数字（ハイフンあり）'
          f.input :address
        end
      end        
    end
    columns do
      column do
        f.inputs '会社ルートページ詳細' do
          f.input :information
        end
      end
    end
    
    f.actions
  end

  
end
