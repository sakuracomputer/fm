# coding: utf-8
12# coding: utf-8
module Fm
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path('../templates', __FILE__)

    puts "=========================================================="
    puts " Fm installation"
    puts "  ・application_controllerの上書き"
    puts "　・継承modelsの作成"
    puts "  ・Devise用controllers, viewsの作成"    
    puts "　・Deviseルーティング設定"
    puts "　・各gemのinstall_generators実行"
    puts "　・Fmのmigrationファイルコピー"
    puts "　・application.rbの設定"
    puts "　・サンプルseeds.rbの作成"
    puts "　・ActiveAdminのリソースを継承modelsで作成"
    puts "　・application.cssの設定"
    puts "　・views/layoutsの作成"
    puts "  ・views/errorsのコピー"
    puts "　・assetsの作成"
    puts "  ・config/locale/active_admin.rbのコピー"
    puts "  ・config/initializers/roles.rbの作成"
    puts "各gemのgeneratorでviewの上書き確認が表示された場合は"
    puts "問題がないことを確認の上、Overwriteを選択してください"
    puts ""
    puts "何らかの例外で失敗した場合は以下で作成ファイルを削除します"
    puts "# bundle exec rails d fm:install"
    puts "ただし、config/routes.rbだけは手動で内容を消去してください"
    puts "=========================================================="

    attr_accessor :force

    def initialize(*args)
      super
      @force = ARGV.include?("-f") ? "-f" : ""
    end

    desc "app/controllers/application_controllerをコピーします"
    def copy_application_controller
      copy_file "controllers/application_controller.rb", "app/controllers/application_controller.rb"
    end
    
    desc "modelsをコピーします"
    def copy_models
      copy_file "models/ability.rb", "app/models/ability.rb"
      copy_file "models/company.rb", "app/models/company.rb"
      copy_file "models/user.rb", "app/models/user.rb"
      copy_file "models/user_role.rb", "app/models/user_role.rb"
    end

    desc "Devise用controllersをコピーします"
    def copy_devise_controllers
       # user
      copy_file "controllers/users/sessions_controller.rb", "app/controllers/users/sessions_controller.rb"
      copy_file "controllers/users/passwords_controller.rb", "app/controllers/users/passwords_controller.rb"

      copy_file "views/users/sessions/new.html.haml", "app/views/users/sessions/new.html.haml"
      copy_file "views/users/passwords/new.html.haml", "app/views/users/passwords/new.html.haml"
      copy_file "views/users/passwords/edit.html.haml", "app/views/users/passwords/edit.html.haml"

      # devise
      copy_file "views/devise/shared/_links.html.haml", "app/views/devise/shared/_links.html.haml"
    end

    desc "Welcome#indexを作成します"
    def create_welcome_controller
      generate "controller", "Welcome index --skip-routes"
    end

    desc "gemのgeneratorsを実行します"
    def install_gems_generators
      generate "simple_form:install --bootstrap #{force}"
      generate "active_admin:install", "--skip-users #{force}"
      generate "bootstrap:install", "--template-engine=haml --stylesheet-engine=scss #{force}"
      generate "rambulance:install", "-e haml #{force}"
    end

    desc "routes.rbに設定を挿入します"
    def config_routes
      route 'mount Fm::Engine => "/fm", as: "fm"'
      route 'root to: "welcome#index"'
      # deviseのルーティングもapplication側で行う
      route <<-'EOS'
devise_for :users, path: 'companies/(:identifier)', class_name: "Fm::User", skip: [:registration], controllers: {
             sessions: "users/sessions",
             passwords: "users/passwords",
           }
  devise_scope :user do
    get "admin_users/sign_in", to: "users/sessions#new"
    get "admin_users/sign_out", to: "users/sessions#destroy"
  end

EOS
      #  ActiveAdmin.routes(self)
    end

    desc "migrationsをコピーします"
    def rake_railties_install_migrations
      rake "railties:install:migrations"
    end

    desc "application.rbに設定を追記します"
    def edit_application
      application do <<-"EOS"
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :ja
    config.i18n.available_locales = [:ja, :en]
    config.time_zone = 'Tokyo'
    config.active_record.default_timezone = :utc

    # add bootstrap Asset Pipeline
    config.assets.precompile = config.assets.precompile + %w(*.png *.jpg *.jpeg *.gif *.woff *.woff2 *.ttf *.svg *.eot)

    config.generators do |g|
      g.template_engine :haml
      g.test_framework :rspec,
                       fixtures: true,
                       view_specs: false,
                       helper_specs: false,
                       routing_specs: false,
                       controller_specs: true,
                       request_specs: false
    end

    # ActiveAdmin
    config.app_generators.scaffold_controller = :scaffold_controller

    # add validators path
    config.autoload_paths += Dir['\#{config.root}/app/validators']
    # add batchs path
    config.autoload_paths += Dir['\#{config.root}/app/batches']

    EOS
      end
    end

    desc "initializers/roles.rbを作成します"
    def create_roles_initializers
      create_file "config/initializers/roles.rb", <<-"EOS"
Fm.setup do |config|
  config.roles = { company_admin: '会社管理者',
                   user: 'ユーザ',
                   power_user: 'パワーユーザ',
                 }
end
EOS
    end

    desc "seeds.rbをコピーします"
    def copy_seeds
      copy_file "db/seeds.rb", "db/seeds.rb"
    end

    desc "ActiveAdminリソースをapp/adminに作成します"
    def copy_admin_resources
      copy_file "admin/company.rb", "app/admin/company.rb"
      copy_file "admin/user.rb", "app/admin/user.rb"
    end

    desc "application.cssを設定します"
    def edit_application_css
      inject_into_file 'app/assets/stylesheets/application.css', before: ' *= require_self' do <<-'EOS'
 *= require bootstrap-generators
EOS
      end
      inject_into_file 'app/assets/stylesheets/application.css', before: ' *= require_self' do <<-'EOS'
 *= require fm/fm
EOS
      end
    end

    desc "application.html.haml, _footer.html.hamlをapp/views/layoutsに作成します"
    def copy_layouts
      copy_file "views/layouts/application.html.haml", "app/views/layouts/application.html.haml", force: (force.blank? ? false : true)
      copy_file "views/layouts/_footer.html.haml", "app/views/layouts/_footer.html.haml", force: (force.blank? ? false : true)
    end

    desc "views/errorsをapp/views/errorsにコピーします"
    def copy_errors_views
      copy_file "views/errors/forbidden.html.haml", "app/views/errors/forbidden.html.haml"
      copy_file "views/errors/not_found.html.haml", "app/views/errors/not_found.html.haml"
      copy_file "views/errors/internal_server_error.html.haml", "app/views/errors/internal_server_error.html.haml"
      copy_file "views/errors/bad_request.html.haml", "app/views/errors/bad_request.html.haml"
      copy_file "views/errors/unprocessable_entity.html.haml", "app/views/errors/unprocessable_entity.html.haml"
    end
    
    desc "assetsをコピーします"
    def copy_assets
      copy_file "assets/images/copyright_logo.png", "app/assets/images/copyright_logo.png"
    end

    desc "gemのinitializersを上書きコピーします"
    def copy_gems_initializers
      copy_file "initializers/devise.rb", "config/initializers/devise.rb", force: true
      copy_file "initializers/active_admin.rb", "config/initializers/active_admin.rb", force: true
      copy_file "initializers/rambulance.rb", "config/initializers/rambulance.rb", force: true
    end

    desc "config/localesを設定します"
    def copy_locales
      copy_file "config/locales/active_admin.ja.yml", "config/locales/active_admin.ja.yml"
    end
  end
  
end
