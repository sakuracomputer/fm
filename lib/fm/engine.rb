# coding: utf-8
module Fm
  class Engine < ::Rails::Engine
    isolate_namespace Fm

    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.fixture_replacement :factory_girl, dir: "spec/factories"

      g.template_engine :haml
      g.helper false
      g.stylesheets false
      g.javascripts false
    end

    # ActiveAdminのInheritedResourceをscaffoldで使わない
    config.app_generators.scaffold_controller = :scaffold_controller

    # add validators path
    config.autoload_paths += Dir["#{config.root}/app/validators"]
    # add batchs path
    config.autoload_paths += Dir["#{config.root}/app/batches"]

     initializer :fm do
      ::Devise.setup do |config|
        require 'devise/orm/active_record'
        config.sign_out_via = :get
      end
    end

  end
end
