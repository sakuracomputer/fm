require "rails"
require "devise"
require "fm/engine"
require "kaminari"
require "cancancan"
require "record_with_operator"
require "activeadmin"
require "rambulance"

module Fm
  mattr_accessor :roles
  
  def self.setup
    yield self
  end
  
end
