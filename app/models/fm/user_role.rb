module Fm
  class UserRole < ActiveRecord::Base
    belongs_to :user
    include Fm::HasOperator

    validates_uniqueness_of :role, scope: :user_id, case_sensitive: false

    before_save { self.role = role.downcase }
  end
end
