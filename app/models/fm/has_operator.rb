# coding: utf-8
module Fm
  # レコード操作者を保存、取得可能にする
  module HasOperator
    extend ActiveSupport::Concern

    included do
      records_with_operator_on :create, :update
      has_one :created_user, class_name: 'Fm::User', primary_key: :created_by, foreign_key: :id
      has_one :updated_user, class_name: 'Fm::User', primary_key: :updated_by, foreign_key: :id
    end
  end
end
