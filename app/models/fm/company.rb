# coding: utf-8
module Fm
  class Company < ActiveRecord::Base
    include Fm::HasOperator
    
    has_many :users, dependent: :destroy
    
    validates_uniqueness_of :identifier, case_sensitive: false
    validates_presence_of :identifier
    validates_presence_of :name
    validates_length_of :identifier, in: 1..32
    validates_length_of :name, in: 1..255
    validates_format_of :identifier, with: /\A[0-9a-zA-Z_-]+\z/, message: '半角英数字、ハイフン、アンダースコアのみ入力可能です'
    validates_length_of :email, in: 0..255, allow_blank: true, allow_nil: true
    validates_format_of :email, with: Devise.email_regexp, allow_blank: true, allow_nil: true
    validates_length_of :contact, in: 0..255, allow_blank: true, allow_nil: true
    validates_format_of :tel1, with: /\A[0-9\-\(\)\s]{0,16}\z/, allow_blank: true, allow_nil: true
    validates_format_of :tel2, with: /\A[0-9\-\(\)\s]{0,16}\z/, allow_blank: true, allow_nil: true
    validates_format_of :fax, with: /\A[0-9\-\(\)\s]{0,16}\z/, allow_blank: true, allow_nil: true
	validates_format_of :zip, with: /\A[0-9\-]{0,8}\z/, allow_blank: true, allow_nil: true
    validates_length_of :address, in: 0..255, allow_blank: true, allow_nil: true
    validates_length_of :information, in: 0..1024, allow_blank: true, allow_nil: true

    before_save { self.identifier = identifier.downcase }
    before_save { self.email = email.downcase if self.email.present? }

    # findオーバーライド
    def self.find(*params)
      if params.select{|param| param.to_s =~ /\A-?\d+(.\d+)?\Z/ }.size == params.size
        # 全てidで指定された
        super
      elsif params.select{|param|
              param.to_s !~ /\A-?\d+(.\d+)?\Z/ &&
                ( param.is_a?(String) || param.is_a?(Symbol) )
            }.size == params.size
        # 全て文字列・シンボルで指定された
        params.map!{|param| param.to_s}
        rel = self.arel_table[:identifier].eq(params.first)
        for i in 1..params.size-1
          rel = rel.or(self.arel_table[:identifier].eq(params[i]))
        end
        records = self.where(rel)
        if records.blank?
          raise ActiveRecord::RecordNotFound, "Couldn't find #{name} with 'identifier' = #{params}"
        else
          if params.size == 1
            records.first
          else
            records
          end
        end
      else
        raise StandardError.new('引数は全てidか全てidentifierで指定する必要があります')
      end
    end

  end
end
