# coding: utf-8
module Fm
  class Ability
    include CanCan::Ability

    def initialize(user)
      user ||= User.new
      cannot :manage, :all
      can :read, Fm::Company
      
      if user.root?
        can :manage, :all
        # 運用管理者、自身の削除は不可
        cannot :destroy, User, id: user.id        
        can :manage, ActiveAdmin::Page, name: "Dashboard"
      else
        if user.company_admin?
          can :manage, Fm::User, company_id: user.company_id
          can :edit, Fm::Company, id: user.company_id
        end
      end
    end
    
  end
end
