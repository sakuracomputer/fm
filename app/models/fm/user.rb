# coding: utf-8
module Fm
  class User < ActiveRecord::Base
    include Fm::HasRole
    include Fm::HasOperator
    
    belongs_to :company    
    has_many :user_roles, dependent: :destroy

    accepts_nested_attributes_for :user_roles, allow_destroy: true, reject_if: :all_blank

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    # :registerable,:recoverable, :validatable
    # 会社コードは強制しない（運用管理者の場合)
    devise :database_authenticatable,
           :trackable, :recoverable,
           authentication_keys: {company_id: false, email: true}

    # 運用管理者の場合、会社コードはnil
    # 通常ユーザの場合、会社コードはnil以外
    validates_absence_of :company_id, if: :root?    
    validates_presence_of :company_id, unless: :root?
    
    validates_presence_of :email
    validates_length_of :email, in: 1..255
    validates_format_of :email, with: Devise.email_regexp, allow_blank: true, if: :email_changed?
    validates_uniqueness_of :email, scope: :company_id, case_sensitive: false
    validates_presence_of :name
    validates_length_of :name, in: 1..255
    validates_confirmation_of :password, message: '再入力と一致しません'
    validates_length_of :password, within: Devise.password_length,
                        message: "#{Devise.password_length.to_a.first}～#{Devise.password_length.to_a.last}文字で入力してください", on: :create
    validates_length_of :password, within: Devise.password_length,
                        message: "#{Devise.password_length.to_a.first}～#{Devise.password_length.to_a.last}文字で入力してください", allow_blank: true, on: :update
    validates_length_of :profile, in: 0..1024, allow_blank: true, allow_nil: true
    validates :is_root, inclusion: {in: [true, false]}
    
    before_save do
      self.email = email.downcase
      self.company_id = nil if self.is_root?
    end

    # role(Symbol,String)指定して抽出
    scope :user_roles_in, -> (roles) {
      Fm::User
        .joins(:user_roles)
        .where(
          fm_user_roles: {
            role: roles
          }
        ) if roles.present?
    }

    scope :root_only, -> {
      where(is_root: true) 
    }

    scope :except_root, -> {
      where(is_root: false)
    }

    scope :has_no_roles, -> {
      except_root
        .where(
          UserRole
          .where(
            UserRole.arel_table[:user_id].eq(User.arel_table[:id])
          ).exists.not
        )
    }
    
    def self.ransackable_scopes(auth_object = nil)
      %i(user_roles_in)
    end

    def self.find_for_database_authentication(warden_conditions)

      if warden_conditions[:company_id].present?
        # 会社管理者としてログイン
        where(email: warden_conditions[:email]).where(company_id: warden_conditions[:company_id]).first
      else
        # 運用管理者としてログイン
        where(email: warden_conditions[:email]).where(company_id: nil).first
      end

    end
  end
end
