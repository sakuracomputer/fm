# coding: utf-8
module Fm
  # User, AdminUserに権限メソッドを追加する
  module HasRole
    extend ActiveSupport::Concern

    # root?メソッド定義
    class_eval <<-EOS, __FILE__, __LINE__ +1
      def root?
        self.is_root?
      end
    EOS

    # initializerの設定に従って、権限メソッド定義
    if Fm.try(:roles).present?
      Fm.roles.each do |k, name|
        class_eval <<-EOS, __FILE__, __LINE__ +1
        def #{k}?
          if self.try(:user_roles).present? && self.user_roles.any?{|r| r.role == '#{k.to_s}'}
            true
          else
            false
          end
        end
      EOS
      end

      class_eval <<-EOS, __FILE__, __LINE__ +1
        def no_roles?
          self.try(:user_roles).blank? && self.root? == false
        end
      EOS
      
      # scope定義
      included do
        Fm.roles.each do |k, name|
          scope :"#{k}_only", -> {
                           Fm::User
                             .joins(:user_roles)
                             .where(
                               fm_user_roles: {
                                 role: k
                               }
                             )
                         }
        end
      end
      
    end
  end
  
end
