module Fm
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action only: [:create, :update] { RecordWithOperator.operator = current_user }

  end
end
