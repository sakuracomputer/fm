require_dependency "fm/application_controller"

module Fm
  class CompaniesController < ApplicationController
    before_action :set_company, only: [:show]
#    before_filter :authenticate_admin_user!, only: [:index, :destroy]
    layout 'application'
    
    def show
    end

    private

    def set_company
      @company = Company
                 .accessible_by(current_ability)
                 .find_by!(identifier: params[:identifier])
    end
    
  end
end
