# coding: utf-8
module InitializeMacros

  #= 操作ユーザ、各マスタの初期データ作成
  def initialize_basic_masters
    initialize_admin_users
    initialize_companies
    initialize_company_admins
    initialize_company_users
    initialize_company_power_users
  end

  def initialize_admin_users
    @admin_user1 = create(:admin_user)
    @admin_user2 = create(:admin_user)
  end

  def initialize_companies
    @company1 = create(:company)
    @company2 = create(:company)
  end

  def initialize_company_admins
    @company1_admin1 = create(:company_admin_user, company: @company1)
    @company1_admin2 = create(:company_admin_user, company: @company1)

    @company2_admin1 = create(:company_admin_user, company: @company2)
    @company2_admin2 = create(:company_admin_user, company: @company2)
  end

  def initialize_company_users
    @company1_user1 = create(:user, company: @company1)
    @company1_user2 = create(:user, company: @company1)

    @company2_user1 = create(:user, company: @company2)
    @company2_user2 = create(:user, company: @company2)
  end

  def initialize_company_power_users
    @company1_power_user1 = create(:power_user, company: @company1)
    @company1_power_user2 = create(:power_user, company: @company1)

    @company2_power_user1 = create(:power_user, company: @company2)
    @company2_power_user2 = create(:power_user, company: @company2)
  end

end
