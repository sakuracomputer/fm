# coding: utf-8
module FeatureMacros

  #= サインイン
  def sign_in(user)
    visit root_path
            
    if user.root?
      visit admin_users_sign_in_path
    else
      visit new_user_session_path(user.company.identifier)
    end

    if user.root?
      check_admin_users_sign_in
    else
      check_users_session_new
    end

    fill_in :user_email, with: user.email
    fill_in :user_password, with: user.password
    
    click_button "ログイン"
  end

  def check_users_session_new
    expect(page.current_url).to match /companies\/.+\/sign_in/
    check_devise_session_new
  end

  def check_admin_users_sign_in
    expect(page.current_url.include?('admin_users/sign_in')).to eq true
    check_devise_session_new    
  end

  #= 会社コード指定なしのユーザ認証画面
  # 認証無しで、認証が必要なページを要求した際のリダイレクト先
  def check_no_companies_sign_in
    expect(page.current_url.include?('companies/sign_in')).to eq true
    check_devise_session_new    
  end
  
  def check_devise_session_new
    expect(page).to have_content "メールアドレス"
    expect(page).to have_content "パスワード"
    expect(page).to have_content "ログイン"
    expect(page).to have_content "パスワードをお忘れですか？"
  end
  
  def sign_out(user)
    click_link "ログアウト"
  end

  #= ログイン画面の確認
  def check_new_user_session
    expect(page).to have_content 'メールアドレス'
    expect(page).to have_content 'パスワード'
  end
  
  #= メニューの確認
  def check_header_and_footer(user)
    expect(page).to have_content user.name

    expect(page).to have_content "Firstmation"
    expect(page).to have_content "All rights reserved."
  end

  def visit_admin_root(admin_user)
    visit admin_root_path
  end

  def check_company_show(company)
    expect(page).to have_content company.name
  end

  def check_admin_root
    find :xpath, '//*[@id="site_title"]/a' # サイトTOP link
    find :xpath, '//*[@id="dashboard"]/a' # ダッシュボードlink
    find :xpath, '//*[@id="companies"]/a' # 会社一覧link
    find :xpath, '//*[@id="users"]/a' # ユーザ一覧link
    find :xpath, '//*[@id="logout"]/a' # 運用管理者ログアウトlink
  end

  def check_admin_companies_index
    # 会社作成
    find :xpath, '//div[@class="action_items"]//a[@href="/admin/companies/new"]'
    # 閲覧、編集
    Company.all.each do |c|
      find :xpath, "//a[@href='/admin/companies/#{c.id}']"
      find :xpath, "//a[@href='/admin/companies/#{c.id}/edit']"
    end
  end

  def check_admin_company_show(company)
    expect(page).to have_content company.name

    showable_company_fields.each do |f|
      value = company.send(f)
      if value.present?
        find(:xpath,"//td[text()='#{value}']|//span[text()='#{value}']")
      end
    end

    # 編集
    find :xpath, "//a[@href='/admin/companies/#{company.id}/edit']"
    # 削除
    find :xpath, "//a[@href='/admin/companies/#{company.id}'][@data-method='delete']"
  end

  def check_admin_company_edit(company)
    expect(page).to have_content "会社 を編集する"

    # 入力項目
    editable_company_inputs.each do |field|
      find(:xpath, "//input[@id='company_#{field}']")
    end
    editable_company_textarea.each do |f|
      find(:xpath, "//textarea[@id='company_#{f}']")
    end
    
  end

  def check_admin_users_index
    # ユーザ作成
    find :xpath, '//div[@class="action_items"]//a[@href="/admin/users/new"]'
    # 閲覧、編集
    User.all.each do |c|
      find :xpath, "//a[@href='/admin/users/#{c.id}']"
      find :xpath, "//a[@href='/admin/users/#{c.id}/edit']"
    end
    
  end

  def check_admin_user_show(user)
    expect(page).to have_content  user.name

    showable_user_fields.each do |f|
      value = user.send(f)
      value.gsub!("'", "\\\\'") if value.respond_to?(:gsub!)
      value = value.html_safe if value.respond_to?(:html_safe)
      if value.present?
        find_all(:xpath,"//td[text()='#{value}']|//span[text()='#{value}']")
      end
    end

    # 編集
    find :xpath, "//a[@href='/admin/users/#{user.id}/edit']"

  end

  def check_admin_user_edit(user)
    expect(page).to have_content "ユーザ を編集する"

    # 入力項目
    editable_user_inputs.each do |field|
      find(:xpath, "//input[@id='user_#{field}']")
    end

  end
  
  private

  def showable_user_fields
    %W(id company_id name is_root email created_user created_at updated_user updated_at)
  end

  def editable_user_inputs
    %W(name email password password_confirmation)
  end
  
  def showable_company_fields
    except_array = %w(id created_at updated_at created_by updated_by)
    Company.column_names - except_array
  end
  
  def editable_company_inputs
    except_array = %w(id created_at updated_at created_by updated_by information)
    Company.column_names - except_array
  end

  def editable_company_textarea
    %w(information)
  end
end
