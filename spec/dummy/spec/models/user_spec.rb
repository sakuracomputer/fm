# coding: utf-8
# == Schema Information
#
# Table name: fm_users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  company_id             :integer
#  name                   :string(255)      default(""), not null
#  is_root                :boolean          default(FALSE), not null
#  profile                :text(65535)
#  created_by             :integer
#  updated_by             :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  subject { @user }
  before do
    @user = build(:user,
                  company: create(:company)
                 )
  end

  describe "全ての項目が存在すること" do
    it { should respond_to(:name) }
    it { should respond_to(:email) }
    it { should respond_to(:password) }
    it { should respond_to(:profile) }
    it { should respond_to(:created_by) }
    it { should respond_to(:updated_by) }

    it { should respond_to(:company) }
    it { should respond_to(:user_roles) }

    it { should respond_to(:root?) }
  end

  describe "入力項目の確認" do
    describe "氏名" do
      context "未入力の場合" do
        it "エラーであること" do
          @user.name = ''
          should_not be_valid
          @user.name = nil
          should_not be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーであること" do
          @user.name = 'a' * 256
          should_not be_valid
          @user.name = 'あ' * 256
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @user.name = 'a' * 255
          should be_valid
          @user.name = 'あ' * 255
          should be_valid
        end
      end
    end

    describe "email" do
      context "未入力の場合" do
        it "エラーであること" do
          @user.email = ''
          should_not be_valid
          @user.email = nil
          should_not be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーであること" do
          @user.email = 'a' * 245 + '@sample.com'
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @user.email = 'a' * 244 + '@sample.com'
          should be_valid
        end
      end

      context "email形式でない場合" do
        it "エラーとなること" do
          @user.email = 'a' * 255
          should_not be_valid
        end
      end

      context "大文字で入力されて保存した場合" do
        it "小文字に変換されること" do
          @user.email = 'Aaa@sample.com'
          @user.save
          expect(@user.email).to match /\A[^A-Z]+\z/
        end
      end

      describe "一意制約" do
        before do
          @same_email_user = @user.dup
          @admin_user = build(:admin_user)
          @same_email_admin_user = @admin_user.dup
          @same_email_admin_user.save!
        end

        describe "運用管理者" do
          context "同じemailの場合" do
            it "エラーであること" do
              expect(@admin_user.valid?).to eq false
            end
          end
        end
        
        describe "運用管理者以外（会社のユーザ）" do
          context "同じ会社で同じemailの場合" do
            it "エラーであること" do
              @same_email_user.save
              should_not be_valid
              @user.email = 'AAA@sample.com'
              @same_email_user.email = 'aaa@sample.com'
              @same_email_user.save
              should_not be_valid
            end
          end

          context "別会社で同じemailの場合" do
            it "エラーとならないこと" do
              other_company = create(:company)
              @same_email_user.company = other_company
              @same_email_user.save
              should be_valid
            end
          end
        end
        
      end
      
    end

    describe "プロフィール" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @user.profile = ''
          should be_valid
          @user.profile = nil
          should be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーとなること" do
          @user.profile = 'a' * 1025
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @user.profile = 'a' * 1024
          should be_valid
          @user.profile = 'あ' * 1024
          should be_valid
        end
      end
    end

    describe "会社" do
      describe "運用管理者" do
        before do
          @admin_user = build(:admin_user)
        end

        context "未入力の場合" do
          it "エラーとならないこと" do
            @admin_user.company_id = nil
            expect(@admin_user.valid?).to eq true
          end
        end
        
        context "入力された場合" do
          it "エラーとなること" do
            @admin_user.company_id = 1
            expect(@admin_user.valid?).to eq false
          end
        end
      end

      describe "運用管理者以外（会社のユーザ）" do
        context "未入力の場合" do
          it "エラーとなること" do
            @user.company_id = nil
            should_not be_valid
          end
        end
      end
    end

    describe "パスワード" do
      context "作成時" do
        context "未入力の場合" do
          it "エラーとなること" do
            @user.password = ''
            should_not be_valid
            @user.password = nil
            should_not be_valid
          end
        end

        context "文字数制限未満の場合" do
          it "エラーとなること" do
            @user.password = '1234567'
            should_not be_valid
          end
        end

        context "文字数制限を越える場合" do
          it "エラーとなること" do
            @user.password = 'a' * 73
            should_not be_valid
          end
        end

        context "再入力と値が異なる場合" do
          it "エラーとなること" do
            @user.password = 'password'
            @user.password_confirmation = 'pasword!!!!!'
            should_not be_valid
          end
        end

        context "再入力と値が一致する場合" do
          it "エラーとならないこと" do
            @user.password = 'password'
            @user.password_confirmation = @user.password
            should be_valid
          end
        end
      end
      
      context "更新時" do
        before do
          @user.save!
          @user.reload
        end

        context "未入力の場合" do
          it "エラーとならないこと" do
            @user.password = ''
            should be_valid
          end
        end

        context "文字数制限未満の場合" do
          it "エラーとなること" do
            @user.password = '1234567'
            should_not be_valid
          end
        end

        context "文字数制限を越える場合" do
          it "エラーとなること" do
            @user.password = 'a' * 73
            should_not be_valid
          end
        end
        
        context "再入力と値が異なる場合" do
          it "エラーとなること" do
            @user.password = 'password'
            @user.password_confirmation = 'pasword!!!!!'
            should_not be_valid
          end
        end

        context "再入力と値が一致する場合" do
          it "エラーとならないこと" do
            @user.password = 'password'
            @user.password_confirmation = @user.password
            should be_valid
          end
        end
        
      end
    end

  end
  
  describe "#root?" do
    it "falseを返すこと" do
      expect(@user.root?).to eq false
    end
  end

  describe "権限" do
    context "権限を一つ持つユーザ" do
      before do
        @company_admin = build(:company_admin_user)
        @user = build(:user)
        @power_user = build(:power_user)
      end

      describe "登録された権限識別子で、その権限を持つかどうかのメソッドが定義されること" do
        it { should respond_to(:company_admin?) }
        it { should respond_to(:user?) }
        it { should respond_to(:power_user?) }
      end

      describe "権限を持つかどうかのメソッド" do
        context "権限を持つユーザの場合" do
          it "trueを返すこと" do
            expect(@company_admin.company_admin?).to eq true
            expect(@user.user?).to eq true
            expect(@power_user.power_user?).to eq true
          end
        end
        context "権限を持たないユーザの場合" do
          it "falseを返すこと" do
            expect(@company_admin.user?).to eq false
            expect(@user.company_admin?).to eq false
            expect(@power_user.company_admin?).to eq false
          end
        end
      end
    end

    context "権限を複数持つユーザ" do
      before do
        # 複数の権限を持つユーザ
        @user_can_be_power_user = build(:user)
        @user_can_be_power_user.user_roles << build(:power_user_role)
      end

      describe "権限を持つかどうかのメソッド" do
        context "権限を持つユーザの場合" do
          it "trueを返すこと" do
            expect(@user_can_be_power_user.user?).to eq true
            expect(@user_can_be_power_user.power_user?).to eq true
          end
        end
        context "権限を持たないユーザの場合" do
          it "falseを返すこと" do
            expect(@user_can_be_power_user.company_admin?).to eq false
          end
        end
      end
    end
  end

  describe "scopes" do
    before do
      create(:admin_user)
      create(:company_admin_user)
      create(:user)
      create(:power_user)
    end
    
    describe "active_admin用の権限指定 ::user_role_in" do

      describe "定義追加の確認" do
        it "scopeが定義されていること" do
          users = User.user_roles_in([:company_admin])
        end
      end
      
      describe "権限指定" do
        context "単一の権限指定" do
          it "指定した権限のユーザが取得できること" do
            users = User.user_roles_in([:company_admin])
            users.each do |u|
              expect(u.company_admin?).to eq true
            end
            expect(users.size).to be > 0
          end
        end
        context "文字列での権限指定" do
          it "指定した権限のユーザが取得できること" do
            users = User.user_roles_in(['company_admin'])
            users.each do |u|
              expect(u.company_admin?).to eq true
            end
            expect(users.size).to be > 0              
          end
          
        end
        context "複数の権限指定" do
          it "指定した複数の権限のユーザが取得できること" do
            users = User.user_roles_in([:company_admin, :power_user])
            company_admins = []
            power_users = []
            users.each do |u|
              expect(u.company_admin? || u.power_user?).to eq true
              company_admins << u if u.company_admin?
              power_users << u if u.power_user?
            end

            expect(company_admins.size).to be > 0
            expect(power_users.size).to be > 0
          end
        end
      end
    end


    describe "権限で抽出するscope" do
      it "定義されていること" do
        users = User.company_admin_only
        expect(users.size).to be > 0
        users.each do |u|
          expect(u.company_admin?).to eq true
        end

        users = User.power_user_only
        expect(users.size).to be > 0
        users.each do |u|
          expect(u.power_user?).to eq true
        end
        
        users = User.user_only
        expect(users.size).to be > 0
        users.each do |u|
          expect(u.user?).to eq true
        end
        
      end
    end

    describe "運用管理者のみ ::root_only" do
      it "定義されていること" do
        users = User.root_only
        expect(users.size).to be > 0
        users.each do |u|
          expect(u.root?).to eq true
        end
      end
    end

    describe "運用管理者以外 ::except_root" do
      it "定義されていること" do
        users = User.except_root
        expect(users.size).to be > 0
        users.each do |u|
          expect(u.root?).to eq false
        end
        
      end
    end
  end
  
end

