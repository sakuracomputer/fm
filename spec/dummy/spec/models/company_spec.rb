# coding: utf-8
# == Schema Information
#
# Table name: fm_companies
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  identifier  :string(255)      not null
#  email       :string(255)
#  contact     :string(255)
#  tel1        :string(255)
#  tel2        :string(255)
#  fax         :string(255)
#  zip         :string(255)
#  address     :string(255)
#  information :text(65535)
#  created_by  :integer
#  updated_by  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  expired_at  :date
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  subject { @company }
  before do
    @company = build(:company)
  end

  describe "全ての項目が存在すること" do
    it { should respond_to(:name) }
    it { should respond_to(:identifier) }
    it { should respond_to(:email) }
    it { should respond_to(:contact) }
    it { should respond_to(:tel1) }
    it { should respond_to(:tel2) }
    it { should respond_to(:zip) }
    it { should respond_to(:address) }
    it { should respond_to(:information) }
    it { should respond_to(:created_by) }
    it { should respond_to(:updated_by) }
    
    it { should respond_to(:users) }
  end

  describe "入力項目の確認" do
    describe "名前" do
      context "未入力の場合" do
        it "エラーであること" do
          @company.name = ''
          should_not be_valid
          @company.name = nil
          should_not be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーであること" do
          @company.name = 'a' * 256
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.name = 'a' * 255
          should be_valid
        end
      end

    end

    describe "識別子" do
      context "未入力の場合" do
        it "エラーとなること" do
          @company.identifier = ''
          should_not be_valid
          @company.identifier = nil
          should_not be_valid
        end
      end

      context "空白を含む場合" do
        it "エラーであること" do
          @company.identifier = 'aaa bbb'
          should_not be_valid
        end
      end

      context "全角文字を含む場合" do
        it "エラーであること" do
          @company.identifier = 'あ'
          should_not be_valid
        end
      end
      
      context "文字数制限を越える場合" do
        it "エラーであること" do
          @company.identifier = 'a' * 33
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.identifier = 'a' * 32
          should be_valid
        end
      end

      context "大文字で入力して保存された場合" do
        it "小文字に変換されること" do
          @company.identifier = 'aBc01-_'
          @company.save
          expect(@company.identifier).to match /\A[a-z0-9_-]+\z/
        end
      end

      describe "一意制約" do
        context "同じ識別子の会社が存在する場合" do
          before do
            @other_company = @company.dup
          end

          it "エラーであること" do
            @other_company.save
            should_not be_valid
            @other_company.identifier = 'aaa'
            @other_company.save
            @company.identifier = 'Aaa'
            should_not be_valid
          end
        end
      end
    end

    describe "email" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.email = ''
          should be_valid
          @company.email = nil
          should be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーであること" do
          @company.email = 'a' * 245 + '@sample.com'
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.email = 'a' * 244 + '@sample.com'
          should be_valid
        end
      end

      context "emailの形式が不正な場合" do
        it "エラーであること" do
          @company.email = 'a' * 255
          should_not be_valid
        end
      end

      context "大文字で入力して保存された場合" do
        it "小文字に変換されること" do
          @company.email = 'abC@sample.com'
          @company.save
          expect(@company.email).to match /\A[^A-Z]+\z/
        end
      end
    end

    describe "担当者" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.contact = ''
          should be_valid
          @company.contact = nil
          should be_valid
        end
      end

      context "文字列制限を越える場合" do
        it "エラーとなること" do
          @company.contact = 'a' * 256
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.contact = 'a' * 255
          should be_valid
          @company.contact = 'さ' * 255
          should be_valid
        end
      end
    end

    describe "電話番号, Fax番号" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.tel1 = ''
          should be_valid
          @company.tel2 = ''
          should be_valid
          @company.fax = ''
          should be_valid
          @company.tel1 = nil
          should be_valid
          @company.tel2 = nil
          should be_valid
          @company.fax = nil
          should be_valid
        end
      end

      context "形式、桁数が不正な場合" do
        describe "電話番号１" do
          it "エラーであること" do
            @company.tel1 = 'a'
            should_not be_valid
            @company.tel1 = '12345678901234567'
            should_not be_valid
            @company.tel1 = '0123-444-555+'
            should_not be_valid
          end
        end
        describe "電話番号２" do
          it "エラーであること" do
            @company.tel2 = 'a'
            should_not be_valid
            @company.tel2 = '----5----0----5--'
            should_not be_valid
            @company.tel2 = '0123-444-555_'
            should_not be_valid
          end
        end
        describe "Fax番号" do
          it "エラーであること" do
            @company.fax = 'a'
            should_not be_valid
            @company.fax = '----5----0----5--'            
            should_not be_valid
            @company.fax = '012_4444_5555'
            should_not be_valid
          end
        end
        
      end
    end

    describe "郵便番号" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.zip = ''
          should be_valid
          @company.zip = nil
          should be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーとなること" do
          @company.zip = '0' * 9
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.zip = '0' * 8
          should be_valid
        end
      end

      context "形式、が不正な場合" do
        it "エラーとなること" do
          @company.zip = 'あ'
          should_not be_valid
          @company.zip = '111-222_'
          should_not be_valid
        end
      end
    end

    describe "住所" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.address = ''
          should be_valid
          @company.address = nil
          should be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーとなること" do
          @company.address = 'a' * 256
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.address = 'a' * 255
          should be_valid
          @company.address = 'あ' * 255
          should be_valid
        end
      end
    end

    describe "備考" do
      context "未入力の場合" do
        it "エラーとならないこと" do
          @company.information = ''
          should be_valid
          @company.information = nil
          should be_valid
        end
      end

      context "文字数制限を越える場合" do
        it "エラーとなること" do
          @company.information = 'a' * 1025
          should_not be_valid
        end
      end

      context "文字数制限を越えない場合" do
        it "エラーとならないこと" do
          @company.information = 'a' * 1024
          should be_valid
          @company.information = 'あ' * 1024
          should be_valid
        end
      end
    end
    
  end
end
