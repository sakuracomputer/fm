# coding: utf-8
# == Schema Information
#
# Table name: fm_user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  role       :string(255)      not null
#  created_by :integer
#  updated_by :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe UserRole, type: :model do
  subject { @user_role }
  before do
    @user = build(:user)
    @user_role = build(:user_role,
                       user: @user)
  end
  
  describe "全ての項目が存在すること" do
    it { should respond_to(:role) }
    it { should respond_to(:user) }
  end
  
end

