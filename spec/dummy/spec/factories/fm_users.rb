# == Schema Information
#
# Table name: fm_users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  company_id             :integer
#  name                   :string(255)      default(""), not null
#  is_root                :boolean          default(FALSE), not null
#  profile                :text(65535)
#  created_by             :integer
#  updated_by             :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

FactoryGirl.define do

  factory :user_base, :class => 'Fm::User' do
    sequence(:company_id) { |n|
      n
    }
    name {Faker::Name.last_name + ' ' + Faker::Name.first_name }
    email {
      Faker::Config.locale = :en
      Faker::Internet.email
    }
    password {Faker::Internet.password}
    is_root false
  end

  factory :admin_user, parent: :user_base do
    company_id nil
    is_root true
  end
  
  factory :user, parent: :user_base do
    after(:build) do |user|
      user.user_roles << build(:user_role)
    end
  end

  factory :company_admin_user, parent: :user_base do
    after(:build) do |user|
      user.user_roles << build(:company_admin_role)
    end
  end
  
  factory :power_user, parent: :user_base do
    after(:build) do |user|
      user.user_roles << build(:power_user_role)
    end
  end
end
