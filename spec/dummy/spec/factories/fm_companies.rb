# == Schema Information
#
# Table name: fm_companies
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  identifier  :string(255)      not null
#  email       :string(255)
#  contact     :string(255)
#  tel1        :string(255)
#  tel2        :string(255)
#  fax         :string(255)
#  zip         :string(255)
#  address     :string(255)
#  information :text(65535)
#  created_by  :integer
#  updated_by  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  expired_at  :date
#

FactoryGirl.define do
  factory :company, :class => 'Fm::Company' do
    name { Faker::Company.name }
    identifier { Faker::Lorem.word.gsub(' ', '') }
    email {
      Faker::Config.locale = :en
      Faker::Internet.email
    }
    contact Faker::Name
    tel1 Faker::PhoneNumber.cell_phone.gsub('.','-')[0..13]
    tel2 Faker::PhoneNumber.cell_phone.gsub('.','-')[0..13]
    fax Faker:: PhoneNumber.cell_phone.gsub('.','-')[0..13]
    zip Faker::Address.zip_code[0..7]
    address "#{Faker::Address.city} #{Faker::Address.street_name} #{Faker::Address.street_address} #{Faker::Address.secondary_address} #{Faker::Address.building_number}"
    information Faker::Lorem.characters(15)
  end

end
