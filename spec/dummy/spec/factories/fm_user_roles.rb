# == Schema Information
#
# Table name: fm_user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  role       :string(255)      not null
#  created_by :integer
#  updated_by :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :company_admin_role, :class => 'Fm::UserRole' do
    role :company_admin
  end

  factory :user_role, parent: :company_admin_role do
    role :user
  end

  factory :power_user_role, class: Fm::UserRole do
    role :power_user
  end

end
