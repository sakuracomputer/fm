# coding: utf-8
require 'rails_helper'

feature 'ユーザ認証' do
  before do
    initialize_basic_masters
  end

  scenario "運用管理者ログイン -> ログアウト -> ActiveAdminルートへ遷移 -> 運用管理者ログインへリダイレクト" do
    sign_in(@admin_user1)
    visit_admin_root(@admin_user1)
    sign_out(@admin_user1)
    visit admin_root_path

    check_no_companies_sign_in
    expect(page).to have_content "運用管理者ログイン"
  end
  
  scenario "一般ユーザログイン -> ActiveAdminルートへ遷移 -> 404" do
    sign_in(@company1_user1)
    visit admin_root_path

    expect(page).to have_content "お探しのページは存在しませんでした"
  end

  scenario "認証なしで認証不要なページの参照可能であること" do
    visit root_path
    visit fm.company_path(@company1.identifier)

    check_company_show(@company1)
  end

end
