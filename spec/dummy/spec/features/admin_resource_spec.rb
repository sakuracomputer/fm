# coding: utf-8
require 'rails_helper'

feature 'activeadminリソース', js: true do
  before do
    initialize_basic_masters
  end

  describe "運用管理者ログイン -> 各リソースページ" do
    before do
      sign_in(@admin_user2)
      visit_admin_root(@admin_user2)
      check_admin_root
    end
    
    scenario "会社を閲覧、編集、削除できること" do
      # 一覧
      find(:xpath, '//*[@id="companies"]/a').click
      check_admin_companies_index

      # 閲覧
      find(:xpath, "//a[@href='/admin/companies/#{@company1.id}']").click
      check_admin_company_show(@company1)

      # 閲覧 -> 編集
      find(:xpath, "//a[@href='/admin/companies/#{@company1.id}/edit']").click
      check_admin_company_edit(@company1)
      fill_in :company_name, with: '会社名変更'
      click_button "会社を更新"
      @company1.reload
      expect(@company1.name).to eq "会社名変更"
      check_admin_company_show(@company1)
            
      # 削除
      click_link "会社 を削除する"
      expect{
        Company.find(@company1.id)
      }.to raise_error  ActiveRecord::RecordNotFound
    end

    describe "ユーザ"  do
      before do
        find(:xpath, '//*[@id="users"]/a').click
        check_admin_users_index
      end

      scenario "運用管理者が自身の閲覧画面を開いた場合、削除ボタンが存在しないこと", current: true do
        # 閲覧
        find(:xpath, "//a[@href='/admin/users/#{@admin_user2.id}']").click
        check_admin_user_show(@admin_user2)

        page.has_no_xpath?("//a[@href='/admin/users/#{@admin_user2.id}'][@data-method='delete']")
      end

      scenario "会社のユーザの閲覧画面を開いた場合、削除ボタンが存在すること" do
        # 閲覧
        # company_admin
        find(:xpath, "//a[@href='/admin/users/#{@company1_admin1.id}']").click
        check_admin_user_show(@company1_admin1)

        page.has_xpath?("//a[@href='/admin/users/#{@company1_admin1.id}'][@data-method='delete']")

        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        check_admin_users_index

        # power_user
        find(:xpath, "//a[@href='/admin/users/#{@company1_power_user1.id}']").click
        check_admin_user_show(@company1_power_user1)

        page.has_xpath?("//a[@href='/admin/users/#{@company1_power_user1.id}'][@data-method='delete']")

        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        check_admin_users_index
        # user
        find(:xpath, "//a[@href='/admin/users/#{@company1_user1.id}']").click
        check_admin_user_show(@company1_user1)

        page.has_xpath?("//a[@href='/admin/users/#{@company1_user1.id}'][@data-method='delete']")
        
      end

      scenario "ユーザを編集できること" do
        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        
        find(:xpath, "//a[@href='/admin/users/#{@admin_user2.id}/edit']").click
        check_admin_user_edit(@admin_user2)
 
        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        
        find(:xpath, "//a[@href='/admin/users/#{@company1_admin1.id}/edit']").click
        check_admin_user_edit(@company1_admin1)

        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        
        find(:xpath, "//a[@href='/admin/users/#{@company1_power_user1.id}/edit']").click
        check_admin_user_edit(@company1_power_user1)

        # 一覧
        find(:xpath, '//*[@id="users"]/a').click
        
        find(:xpath, "//a[@href='/admin/users/#{@company1_user2.id}/edit']").click
        check_admin_user_edit(@company1_user2)

      end
      # # 閲覧 -> 編集
      # find(:xpath, "//a[@href='/admin/users/#{@admin_user2.id}/edit']").click
      # check_admin_company_edit(@company1)

      # fill_in :company_name, with: '会社名変更'
      # click_button "会社を更新"
      # @company1.reload
      # expect(@company1.name).to eq "会社名変更"
      # check_admin_company_show(@company1)
            
      # # 削除
      # click_link "会社 を削除する"
      # expect{
      #   Company.find(@company1.id)
      # }.to raise_error  ActiveRecord::RecordNotFound

    end
    
  end


end
