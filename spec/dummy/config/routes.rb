# coding: utf-8
Rails.application.routes.draw do
  devise_for :users, path: 'companies/(:identifier)', class_name: "Fm::User", skip: [:registration], controllers: {
             sessions: "users/sessions",
             passwords: "users/passwords",
           }
  devise_scope :user do
    get "admin_users/sign_in", to: "users/sessions#new"
    get "admin_users/sign_out", to: "users/sessions#destroy"
  end


  root to: "welcome#index"
  mount Fm::Engine => "/fm", as: "fm"
  ActiveAdmin.routes(self)
end
