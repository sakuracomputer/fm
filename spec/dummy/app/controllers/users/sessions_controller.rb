# coding: utf-8
class Users::SessionsController < Devise::SessionsController
  before_filter :configure_sign_in_params, only: [:new, :destroy, :create]
  before_filter only: [:new, :destroy] {
    if params[:identifier].present?
      @company = Company.find(params[:identifier])
    end
  }
  
  # GET /resource/sign_in
  def new
    super do |u|
      u.company_id = @company.try(:id)
    end
  end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  def failed

    if params[:user].present? && params[:user][:company_id].present?
      @company = Company.find(params[:user][:company_id])
    end
    flash[:error] = '入力された情報は存在しませんでした。'
    if @company.present?
      redirect_to new_user_session_path(@company.identifier)
    else
      redirect_to admin_users_sign_in_path
    end
  end

  protected

  def auth_options
    { scope: resource_name, recall: "#{controller_path}#failed" }
  end

  def after_sign_in_path_for(resource)
    if resource.root?
      session[:previous_url] ||main_app.admin_root_path
    elsif resource.try(:company).present?
      session[:previous_url] || fm.company_path(resource.company.identifier)
    end
  end

  def after_sign_out_path_for(resource)
    if resource.try(:company).present?
      fm.company_path(resource.company.identifier)
    else
      main_app.root_path
    end
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_in_params
    devise_parameter_sanitizer.for(:sign_in) << :company_id
  end
end
