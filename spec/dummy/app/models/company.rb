# coding: utf-8
# == Schema Information
#
# Table name: fm_companies
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  identifier  :string(255)      not null
#  email       :string(255)
#  contact     :string(255)
#  tel1        :string(255)
#  tel2        :string(255)
#  fax         :string(255)
#  zip         :string(255)
#  address     :string(255)
#  information :text(65535)
#  created_by  :integer
#  updated_by  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  expired_at  :date
#

class Company < Fm::Company
end
