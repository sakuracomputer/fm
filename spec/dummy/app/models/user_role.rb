# coding: utf-8
# == Schema Information
#
# Table name: fm_user_roles
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  role       :string(255)      not null
#  created_by :integer
#  updated_by :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserRole < Fm::UserRole
end
