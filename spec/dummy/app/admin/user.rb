# coding: utf-8
ActiveAdmin.register User do

  Fm.roles.each do |role, name|
    scope :"#{role}_only"
  end
  scope :except_root
  scope :root_only
  
  permit_params do
    User.column_names.concat([:password, :password_confirmation, user_roles_attributes: [:id, :role, :_destroy]])
  end

  menu priority: 3, label: "ユーザ"

  index do
    selectable_column
    column :id
    column :company
    column :is_root
    column :user_roles do |user|
      role_string = ul do
        user.user_roles.map do |r|
          li r.role
        end
      end
      role_string
    end
    column :name
    column :email
    column :updated_at
    column :updated_user
    
    actions defaults: false do |u|
      link = link_to(t('active_admin.view'), admin_user_path(u))
      link = link + '　'
      link = link + link_to(t('active_admin.edit'), edit_admin_user_path(u))
      link
    end
    
  end

  filter :company
  filter :user_roles_in, as: :select,
         collection: proc {Fm.roles}
  filter :name
  filter :email
  filter :updated_at
  filter :updated_user
  
  show do |user|
    attributes_table do
      row :id
      row :company
      row :name
      row :is_root do
        css_class = user.root? ? "status_tag yes" : "status_tag no"
        inner_html = user.root? ? "はい" : "いいえ"
        span class: css_class do
          inner_html
        end
      end
      row :user_roles do
        role_string = ul do
          user.user_roles.map do |r|
            li r.role
          end
        end
        role_string
      end
      row :email
      row :current_sign_in_at
      row :current_sign_in_ip
      row :sign_in_count
      row :last_sign_in_at
      row :last_sign_in_ip
      row :updated_user
      row :updated_at
      row :created_user
      row :created_at
      
    end
  end
  
  form do |f|
    # 運用管理者は会社選択無し
    unless f.object.root?
      columns do
        column do
          f.inputs '会社' do
            f.input :company, hint: '会社のユーザの場合は選択必須です'
          end
        end
        column do; end
      end
    end

    
    columns do
      column do
        f.inputs '認証' do
          f.input :name            
          f.input :email
          f.input :password
          f.input :password_confirmation
        end
      end
      column do

        f.inputs '運用管理者' do
          if f.object.new_record?
            f.input :is_root, hint: '運用管理者にチェックをした場合は、権限割当は不要です'
          else
            f.input :is_root, input_html: {disabled: true}
          end
        end

        if f.object.new_record? or f.object.root? == false
          div id: :role_assign_field do
            f.inputs '権限割当' do
              f.has_many :user_roles, heading: '',  allow_destroy: true, new_record: true do |r|
                r.input :role, as: :select, label: '権限',
                        collection: Fm.roles.invert
              end
            end
          end
        end
      end
    end      

    f.actions
  end

end
