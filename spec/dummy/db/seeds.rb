# coding: utf-8
Fm::User.create!(
  name: 'FirstMation管理者',
  email: 'root@sample.com',
  password: 'password',
  is_root: true
)

Fm::Company.create!(
  name: '会社1',
  identifier: 'test_company',
  email: 'admin@company1.com',
  contact: '担当者A',
  tel1: '000-1111-2222',
  tel2: '000-1111-2223',
  fax: '000-1111-2224',
  address: '大阪府大阪市西淀川区＊＊10番1',
  information: '備考テスト入力'
)

Fm::Company.create!(
  name: '会社2',
  identifier: 'test_company2',
  email: 'admin@company2.com',
  contact: '担当者B',
  tel1: '000-1111-5555',
  tel2: '000-1111-5556',
  fax: '000-1111-6667',
  address: '大阪府大阪市西淀川区＊＊10番1'
)
  
Fm::User.create!(
  company: Fm::Company.first,
  name: 'テストユーザ',
  email: 'test@sample.com',
  password: 'password',
  user_roles_attributes: [
    {
      role: :user,
    },
  ]
)

Fm::User.create!(
  company: Fm::Company.second,
  name: 'テストユーザ(会社1にも会社2にもいる)',
  email: 'test@sample.com',
  password: 'password',
  user_roles_attributes: [
    {
      role: :user,
    },
    {
      role: :power_user,
    },
  ]
)

Fm::User.create!(
  company: Fm::Company.first,
  name: '会社1管理者',
  email: 'admin@company1.com',
  password: 'password',
  user_roles_attributes: [
    {
      role: :company_admin,
    },
  ]
)

Fm::User.create!(
  company: Fm::Company.second,
  name: '会社2管理者',
  email: 'admin@company2.com',
  password: 'password',
  user_roles_attributes: [
    {
      role: :company_admin,
    },
  ]
)

