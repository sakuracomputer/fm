# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151111043554) do

  create_table "fm_companies", force: :cascade do |t|
    t.string   "name",        limit: 255,   null: false
    t.string   "identifier",  limit: 255,   null: false
    t.string   "email",       limit: 255
    t.string   "contact",     limit: 255
    t.string   "tel1",        limit: 255
    t.string   "tel2",        limit: 255
    t.string   "fax",         limit: 255
    t.string   "zip",         limit: 255
    t.string   "address",     limit: 255
    t.text     "information", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.date     "expired_at"
  end

  add_index "fm_companies", ["identifier"], name: "index_fm_companies_on_identifier", unique: true, using: :btree

  create_table "fm_user_roles", force: :cascade do |t|
    t.integer  "user_id",    limit: 4,   null: false
    t.string   "role",       limit: 255, null: false
    t.integer  "created_by", limit: 4
    t.integer  "updated_by", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "fm_user_roles", ["user_id", "role"], name: "index_fm_user_roles_on_user_id_and_role", unique: true, using: :btree

  create_table "fm_users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",    null: false
    t.string   "encrypted_password",     limit: 255,   default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "company_id",             limit: 4
    t.string   "name",                   limit: 255,   default: "",    null: false
    t.boolean  "is_root",                              default: false, null: false
    t.text     "profile",                limit: 65535
    t.integer  "created_by",             limit: 4
    t.integer  "updated_by",             limit: 4
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  add_index "fm_users", ["company_id", "email"], name: "index_fm_users_on_company_id_and_email", unique: true, using: :btree
  add_index "fm_users", ["company_id"], name: "index_fm_users_on_company_id", using: :btree
  add_index "fm_users", ["reset_password_token"], name: "index_fm_users_on_reset_password_token", unique: true, using: :btree

end
