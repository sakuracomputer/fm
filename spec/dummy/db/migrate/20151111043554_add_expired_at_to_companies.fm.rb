# This migration comes from fm (originally 20151101065848)
class AddExpiredAtToCompanies < ActiveRecord::Migration
  def change
    add_column :fm_companies, :expired_at, :date
  end
end
