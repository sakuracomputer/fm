# This migration comes from fm (originally 20151015124105)
class CreateFmUserRoles < ActiveRecord::Migration
  def change
    create_table :fm_user_roles do |t|
      t.references :user, null: false
      t.string :role, null: false
      t.integer :created_by
      t.integer :updated_by
      
      t.timestamps null: false
    end

    add_index :fm_user_roles, [:user_id, :role], unique: true
  end
end
