# This migration comes from fm (originally 20151013124106)
class CreateFmCompanies < ActiveRecord::Migration
  def change
    create_table :fm_companies do |t|
      t.string :name, null: false
      t.string :identifier, null: false
      t.string :email
      t.string :contact
      t.string :tel1
      t.string :tel2
      t.string :fax
      t.string :zip
      t.string :address
      t.text :information
      t.integer :created_by
      t.integer :updated_by

      t.timestamps null: false
    end

    add_index :fm_companies, :identifier, unique: true
  end
end
