Fm::Engine.routes.draw do

  resources :companies, only:[:show, :edit, :update], param: :identifier do
    resources :users
  end

end
