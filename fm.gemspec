# coding: utf-8
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "fm/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "fm"
  s.version     = Fm::VERSION
  s.authors     = ["sakura-computer"]
  s.email       = ["ishimoto.masaru@sakura-computer.co.jp"]
  s.homepage    = "https://bitbucket.org/sakuracomputer/fm"
  s.summary     = "FirstMation向けコアengine"
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]
  
  s.required_ruby_version = "~>2.2"
  s.add_development_dependency "bundler", "~> 1.10"
  s.add_development_dependency "rake", "~> 10.0"
  s.add_development_dependency "rspec", "~> 3.3"
  s.add_development_dependency "rspec-rails", "~> 3.3"
  s.add_development_dependency "factory_girl_rails", "~> 4.0"
  
  s.add_dependency "rails", "~> 4.2.4"
  s.add_dependency "execjs", "~> 2.6"
  s.add_dependency "jquery-turbolinks", "~> 2.1"
  s.add_dependency "rails-i18n", "~> 4.0"
  s.add_dependency "devise", "~> 3.5"
  s.add_dependency "cancancan", "~> 1.12"
  s.add_dependency "draper", "~> 2.1"
  s.add_dependency "kaminari", "~> 0.16"
  s.add_dependency "record_with_operator", "~> 1.0"
  s.add_dependency "activeadmin", "~> 1.0.0.pre2"
  s.add_dependency "simple_form", "~> 3.1"
  s.add_dependency "bootstrap-generators", "~> 3.3"
  s.add_dependency "spinjs-rails", "~> 1.4"
  s.add_dependency "annotate", "~> 2.6"
  s.add_dependency "rambulance", "~> 0.3"

end
