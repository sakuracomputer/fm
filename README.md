# Fm
FirstMation向け  
Devise認証とActiveAdminをバンドルしたmulti-tenancyのRails engine スケルトン

## How to use

**Gemfile**  
bitbucketのsnippetを参考に記述して、Fmを追記。  
```
# FirstMationコア
gem 'fm', git: 'https://bitbucket.org/sakuracomputer/fm'
```
**bundler**  
```
bundle update && bundle install --path=vendor/bundle
```

**database.yml**  
bitbucketのsnippetを参考に記述。

**.gitignore**  
bitbucketのsnippetから追記。

**db create**  
```
bundle exec rake db:create
```

**generator**
各gemで生成するviewで上書き確認が出るが、問題ないことを確認してOverwriteする。  
```
bundle exec rails g fm:install
```
**失敗したら...**  
```
bundle exec rails d fm:install
```
で生成ファイルを削除するが、activeadminのインストールのうち戻せないものあり。  
routes.rb  
```
ActiveAdmin.routes(self)
```
この行は手動で削除しないと、再度fm:install時に例外が発生する。
（Rails4.X以降で定義済みのルーティングを再定義する際にエラーとなるようになったから）  

**ActiveAdminとのcssコンフリクト修正**  
app/assets/stylesheets/application.scssにリネームして以下を除去。
```
require tree .
```

**deploy注意点**  
migrate前にengineのmigrationsをコピーする必要あり  
```
bundle exec rake railties:install:migrations
bundle exec rake db:migrate
```

---------

## Test
spec/dummyでrspecを実行する。
generatorのテストは以下でFm削除・Fmインストールを実行して行う。  
```
./.fm_reinstall
```

migrationを再実行するには以下。（ActiveAdminの不要なmigrationファイル削除後にrake db:migrate:resetする）
```
./.migrate_reset
```

---------

# Details

## Included models

| model | 内容 |関連|
|:------|:-----|:----|
|Company|会社FirstMationの顧客|has_many :users|
|User|ユーザ|belongs_to :company; has_many :user_roles|
|UserRole|ユーザの権限|has_many :users|

Deviseの認証とActiveAdminの初期設定までengine側でやっている。
子applicationで個別指定が必要な場合initializerで行う。
~~Deviseのルーティングはengineでやっているが、ActiveAdminのルーティング設定は子applicationで行う必要あり。~~
generatorでapplication側にルーティング定義するように変更。
{engine_root}/spec/dummy/config/routes.rb
```
Rails.application.routes.draw do
  root to: "welcome#index"

# ActiveAdminのルートはここで定義
  ActiveAdmin.routes(self)    
  ...
```

理由としてはActiveAdminがengineにネストされ得る構成になっていないから。
~~AdminUserでのセッションログアウトのurlヘルパー指定をengine側で行っているが~~
AdminUserモデル廃止、Userに統合。

* シンボルでのengine側urlヘルパーを使えない
* URI直接指定だとHTTPメソッドがdeleteにならない

等問題があるので、Deviseでのログアウトはdeleteではなくgetを指定しているので注意。  
{engine_root}/lib/fm/engine.rb
```
module Fm
  class Engine < ::Rails::Engine
    isolate_namespace Fm

	...

    initializer :fm do
      ::Devise.setup do |config|
	    ...
        config.sign_out_via = :get
		...
	  end

```

具体的にはviewでのmethod指定をget（default: getなので、もしくは指定ない）にする必要がある。
```
     = link_to :logout, fm.destroy_user_session_path(current_user.company_id), method: :get, data: { confirm: "ログアウトしますか？" } 
```

------

## migration
engine側のmigrationは必要に応じて以下でapplication側にコピーする。
```
bundle exec rake railsties:install:migrations
```

------

## activeadmin
application側で継承モデルを使用するため以下は不要。  
~~リソースの定義で、デフォルトではモデル名の名前空間を自動で扱ってくれないため、以下の定義が必要~~
```
ActiveAdmin.register Fm::Hoge do
  controller do
    resources_configuration[:self][:instance_name] = 'hoge'
  end
end
```
~~デフォルトで使用すると更新ボタンを押してもレコードが更新されない。~~
https://github.com/activeadmin/activeadmin/issues/3423

~~更新の際に、パラメータとpermit_paramsの付き合わせが行われる際に、パラメータの取得キーがリソース名（上記だと'hoge'）となるが
モデル名に名前空間が修飾されていた場合、'fm_hoge' <=> 'hoge'でアンマッチとなり、すべてunpermitted parameter扱いになってしまう。~~
------
# engineについて

## engine
Rails engineはRails applicationにマウントして使用されるRackアプリ。自律起動できないapplicationみたいなもの。  
applicationの場合のconfig/application.rbがengineではlib/{engine}/engine.rbになる。  
（lib/{engine}.rb内でrequireされている）
gemとなるので、依存性はGemfileではなく{engine}.gemspecに記述する。
Gemfileに記述してもbundlerがインストールしてくれるだけで、gemの依存定義にはならない（＝gemspecでのgitリポジトリ指定できない）ので注意。

### engine作成時のコマンド
```
rails plugin new fm --mountable -T --dummy-path=spec/dummy
```
--mountableでrailsにマウント可能なengineとして指定、--dummy-pathでspec使用時のテストapplicationpath指定。


### 名前空間
lib/{engine}/engine.rb
```
module Fm
  class Engine < ::Rails::Engine
    isolate_namespace Fm
  end
end
```
isolate_namespaceで子アプリケーションからは名前空間で分離される。
上記の場合は「Fmが名前空間となるため、engine側のクラスにアクセスするには修飾が必要。

{engine_root}/app/models/{engine}/user.rb
```
module Fm
  class User < ActiveRecord::Base
  end
end
```
の場合、Fm::Userとなるため注意。
generatorで継承モデルを作成しているためUserでもアクセス可能。

### rakeタスク
engineルートではrakeタスク名が変わるため注意。「app」での修飾が必要。  
rake -T
```
...
rake app:routes  # Print out all defined
...
```

### テストapplication
{engine_root}/test/dummy (rspec使用の場合は、rspec/dummy)にテスト用のapplicationが作成される。  
テストapplicationではconfig/application.rbにrequire {engine}が自動で挿入されている。  
テストapplicationのGemfileにpath:指定でengineのgemを指定すると、engine変更の度のbundle installする必要なくて楽。  
{engine_root}/spec/dummy/Gemfile
```
# firstmationコア
gem 'fm', path: '../../../fm'
```

ただし、gemでの使用（Gemfileにgitを指定して使用する場合）の際は以下。  
```
bundle udpate fm && bundle install --path=vendor/bundle
```

### ルーティング
engine側のルーティングが定義可能。  
{engine_root}/config/routes.rb
```
Fm::Engine.routes.draw do
  resources :companies, only:[:index, :show] do
    resources :users
  end
end
```

子applicationのルーティングでengineをマウントする。  
{engine_root}/spec/dummy/config/routes.rb
```
Rails.application.routes.draw do
  root to: "welcome#index"
  mount Fm::Engine => "/fm", as: 'fm'
end
```
子application側でルーティングを確認すると、マウントしているengineのルーティングも表示される。
```
# bundle exec rake routes
            Prefix Verb   URI Pattern                   Controller#Action
              root GET    /                             welcome#index

(...ここまで子applicationのルーティング、以降engineのルーティング)

Routes for Fm::Engine:
       company_users GET    /companies/:identifier/users(.:format)          fm/users#index
                    POST    /companies/:identifier/users(.:format)          fm/users#create
    new_company_user GET    /companies/:identifier/users/new(.:format)      fm/users#new

(...)
```
子application側からengineのurlヘルパーを使うには名前空間で修飾が必要。
```
  fm.company_users_path
```
engine側で子applicationのurlヘルパーを使うには下記。
```
  main_app.root_path
```
------

## engine側の設定（作成後にengineルートで実行したこと）
gemspecの依存gemをインストール
```
bundle update && bundle install --path=vendor/bundle
```

### engine.rb
```
    config.generators do |g|
      g.template_engine :haml
      g.helper false
      g.stylesheets false
      g.javascripts false
    end

    (...以降ActiveAdmin,Deviseの設定)
```

### rspec
```
rails g rspec:install
rspec --init
```

### devise
```
rails g devise:install
rails g devise User
```
lib/engine.rbに設定記述。
```
      ::Devise.setup do |config|
        require 'devise/orm/active_record'
        config.sign_out_via = :get
      end

```

------

## application側の設定（spec/dummyルートで実行したこと）
bundle exec rails g fm:install で行っていることを下記。

### application.rb
bitbucketのsnippetを参考に。

### simple_form
```
bundle exec rails g simple_form:install
```

### activeadmin
```
bundle exec rails g active_admin:install --skip-users
```

### bootstrap
```
bundle exec rails g bootstrap:install --template-engine=haml --stylesheet-engine=scss
```

### migration
```
bundle exec rake railties:install:migrations
bundle exec rake db:migrate
```

### seed
{engine_root}/spec/dummy/db/seeds.rbをコピー。

### root_path
```
bundle exec rails g controller Welcome index
```
routes.rbに追記。
```
  root to: "welcome#index"
  get '*path', to: 'fm/application#render_404'
```
### routes.rb
Deviseはengine側、ActiveAdminはapplication側。

app/assets/stylesheets/application.scssに追記
```
 *= require bootstrap-generators
 *= require fm/fm
```
app/assets/javascripts/application.jsになければ追記
```
//= require jquery
```

---------

# LICENSE

This project rocks and uses MIT-LICENSE.